import Header from "@/components/Header";
import React from "react";
import Providers from "../Providers";

export default function About() {
  return (
    <Providers>
      <Header />
      <main className="max-w-6xl mx-auto space-y-4 p-4">
        <h1 className="mx-3 sm:mx-6 font-bold text-3xl text-amber-500 capitalize">
          About this site
        </h1>
        <p className="mx-3 sm:mx-6 text-lg">
          Welcome to our website dedicated to all things movies! Here, you'll
          find a wealth of information about the latest and greatest films from
          around the world.{" "}
        </p>
        <p className="mx-3 sm:mx-6 text-lg">
          Our team of passionate movie enthusiasts works tirelessly to provide
          you with the most comprehensive coverage of the film industry. From
          blockbuster hits to indie gems, we've got you covered. You'll find
          reviews, trailers, interviews with filmmakers and actors, and much
          more on our site.
        </p>
        <p className="mx-3 sm:mx-6 text-lg">
          We hope you enjoy our site and come back often to see what's new. If
          you have any questions or comments, please feel free to contact us.
          We'd love to hear from you!
        </p>
      </main>
    </Providers>
  );
}
