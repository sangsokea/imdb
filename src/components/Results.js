import React from "react";

export default function Results({ results }) {
  return (
    <div>
      {results && results.map((result) => (
        <div key={result.id}>
          <h1>{result.title}</h1>
          <img
            src={`https://image.tmdb.org/t/p/original/${result.poster_path}`}
            alt={result.title}
          />
        </div>
      )) || <div></div>}
    </div>
  );
}


